import React, { Component } from 'react';
import axios from './axios-api';

class App extends Component {
  state = {
    mouseDown: false,
    pixelsArray: [],
    images: []
  };

  componentDidMount() {
    axios.get('/images')
      .then(result => {
        result.data.map(img => {
          this.drawImage(img.image);
        });

        this.setState({images: result.data});
      })
      .catch(error => {
        console.log(error);
      });

    this.websocket = new WebSocket('ws://localhost:8000/canvas');

    this.websocket.onmessage = (message) => {
      const decodedMessage = JSON.parse(message.data);

      let images = [...this.state.images];
      images.push(decodedMessage.text);
      this.drawImage(decodedMessage.text);

      this.setState({images});
    }
  };

  canvasMouseMoveHandler = event => {
    if (this.state.mouseDown) {
      event.persist();

      this.setState(prevState => {
        return {
          pixelsArray: [...prevState.pixelsArray, {
            x: event.clientX,
            y: event.clientY
          }]
        }
      });
    }
  };

  mouseDownHandler = () => {
    this.setState({mouseDown: true});
  };

  mouseUpHandler = () => {
    const newMessage = {type: 'CREATE_MESSAGE', text: this.state.pixelsArray};
    this.websocket.send(JSON.stringify(newMessage));

    this.setState({mouseDown: false, pixelsArray: []});
  };

  drawImage = image => {
    const context = this.canvas.getContext('2d');

    image.forEach(pixel => {
      const imageData = context.createImageData(1, 1);
      const d = imageData.data;

      d[0] = 0; d[1] = 0; d[2] = 0; d[3] = 255;

      context.putImageData(imageData, pixel.x, pixel.y);
    });
  };

  render() {
    return (
      <div className="App">
        <canvas
          ref={elem => this.canvas = elem}
          style={{border: '1px solid black'}}
          width={800}
          height={600}
          onMouseDown={this.mouseDownHandler}
          onMouseUp={this.mouseUpHandler}
          onMouseMove={this.canvasMouseMoveHandler}
        />
      </div>
    );
  }
}

export default App;